import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayerSucessPage } from './player-sucess';

@NgModule({
  declarations: [
    PlayerSucessPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayerSucessPage),
  ],
})
export class PlayerSucessPageModule {}
