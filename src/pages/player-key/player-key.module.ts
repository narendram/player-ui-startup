import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayerKeyPage } from './player-key';

@NgModule({
  declarations: [
    PlayerKeyPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayerKeyPage),
  ],
})
export class PlayerKeyPageModule {}
