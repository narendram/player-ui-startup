import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoAssetsPage } from './no-assets';

@NgModule({
  declarations: [
    NoAssetsPage,
  ],
  imports: [
    IonicPageModule.forChild(NoAssetsPage),
  ],
})
export class NoAssetsPageModule {}
