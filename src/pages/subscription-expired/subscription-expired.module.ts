import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionExpiredPage } from './subscription-expired';

@NgModule({
  declarations: [
    SubscriptionExpiredPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionExpiredPage),
  ],
})
export class SubscriptionExpiredPageModule {}
