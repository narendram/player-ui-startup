import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BootScreenPage } from './boot-screen';

@NgModule({
  declarations: [
    BootScreenPage,
  ],
  imports: [
    IonicPageModule.forChild(BootScreenPage),
  ],
})
export class BootScreenPageModule {}
