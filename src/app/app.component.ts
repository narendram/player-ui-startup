import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BootScreenPage } from '../pages/boot-screen/boot-screen';
import { PlayerSucessPage } from '../pages/player-sucess/player-sucess';
import { NoAssetsPage } from '../pages/no-assets/no-assets';
import { SubscriptionExpiredPage } from '../pages/subscription-expired/subscription-expired';
import { PlayerKeyPage } from '../pages/player-key/player-key';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = BootScreenPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'BootScreen', component: BootScreenPage },
      { title: 'Sucess', component: PlayerSucessPage },
      { title: 'No Assets', component: NoAssetsPage },
      { title: 'Subscription Expired', component: SubscriptionExpiredPage },
      { title: 'Player Key', component: PlayerKeyPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
