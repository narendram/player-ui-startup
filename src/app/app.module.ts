import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { BootScreenPage } from '../pages/boot-screen/boot-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PlayerSucessPage } from '../pages/player-sucess/player-sucess';
import { NoAssetsPage } from '../pages/no-assets/no-assets';
import { SubscriptionExpiredPage } from '../pages/subscription-expired/subscription-expired';
import { PlayerKeyPage } from '../pages/player-key/player-key';

@NgModule({
  declarations: [
    MyApp,
    BootScreenPage,
    PlayerSucessPage,
    NoAssetsPage,
    SubscriptionExpiredPage,
    PlayerKeyPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BootScreenPage,
    PlayerSucessPage,
    NoAssetsPage,
    SubscriptionExpiredPage,
    PlayerKeyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
